﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CExplosiveSpawner : MonoBehaviour {

    [SerializeField, Header("Config")]
    Transform[] _spawnPositions;

    [SerializeField]
    float _bombDelay = 2; // reload time between explosives

    float _bombDelayTimer;

    [SerializeField]
    GameObject _bombPrefab;
    
    [SerializeField]
    GameObject _parentHitBox; // to prevent explosives collisions with parent

    [SerializeField]
    CCar _parentCarScript; // to check if is dead

    [SerializeField, Header("Explosive Pool")]
    int _instancesPerExplosive;

    public List<GameObject> _explosivesPool; // performance

    public State _spawnerType;

    public enum State
    {
        DROPBOMB,
        PROJECTILE
    }

    protected virtual void Start()
    {
        _parentCarScript = _parentHitBox.GetComponentInParent<CCar>();
        _bombDelayTimer = 0;

        // load the pool
        for (int i = 0; i < _instancesPerExplosive; i++)
        {
            GameObject tExplosive = Instantiate(_bombPrefab);
            _explosivesPool.Add(tExplosive);            
        }
    }

    protected virtual void Update()
    {
        _bombDelayTimer += Time.deltaTime;
    }

    public void ReturnExplosiveToPool(GameObject aUsedObject)
    {
        _explosivesPool.Add(aUsedObject);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == CConstants.HITBOX_TAG) // is a car on target position?
        {
            if (!_parentCarScript.IsDead() && _bombDelayTimer >= _bombDelay) // if my car is not dead & the explosive is ready, FIRE!
            {
                int tSelectedSpawnPositionIndex = 0;
                if (_spawnPositions.Length > 1)
                {
                    tSelectedSpawnPositionIndex = Random.Range(0, _spawnPositions.Length);
                }

                // init the explosive
                GameObject tExplosive = _explosivesPool[0];
                _explosivesPool.Remove(tExplosive);                

                // set parent hitbox to prevent collision with it
                switch (_spawnerType)
                {
                    case State.DROPBOMB:
                        CDropBomb tBombScript = tExplosive.GetComponent<CDropBomb>();
                        tBombScript.SetParentHitBox(_parentHitBox);
                        tBombScript.ResetBomb();
                        break;
                    case State.PROJECTILE:
                        CRocket tRocketScript = tExplosive.GetComponent<CRocket>();
                        tRocketScript.SetParentHitBox(_parentHitBox);
                        tRocketScript.ResetBomb();
                        break;
                    default:
                        break;
                }

                tExplosive.transform.position = _spawnPositions[tSelectedSpawnPositionIndex].position;
                tExplosive.transform.rotation = _spawnPositions[tSelectedSpawnPositionIndex].rotation;

                tExplosive.SetActive(true);                

                _bombDelayTimer = 0; // start reload
            }               
        }
    }
}
