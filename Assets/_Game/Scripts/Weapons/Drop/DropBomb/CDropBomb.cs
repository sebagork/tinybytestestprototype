﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CDropBomb : CExplosive {   

    // to return it to the pool
    public CBombSpawner _spawnerReference;

    private void FixedUpdate()
    {
        // move backward
        transform.Translate((-Vector3.forward * 50) * Time.fixedDeltaTime, Space.World);

        // check life time
        _lifeTimer += Time.deltaTime;
        if (_lifeTimer >= _lifeTime)
        {
            // return to the pool
            this.gameObject.SetActive(false);
            _spawnerReference.ReturnExplosiveToPool(this.gameObject);
        }
    }    
}
