﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBombSpawner : CExplosiveSpawner
{
    // specific code for drop bomb spawners

    protected override void Start()
    {
        _spawnerType = State.DROPBOMB;
        base.Start();

        // set spawner references
        for (int i = 0; i < _explosivesPool.Count; i++)
        {
            _explosivesPool[i].GetComponent<CDropBomb>()._spawnerReference = this;
        }
    }

    protected override void Update()
    {
        base.Update();
        // ...
    }
}
