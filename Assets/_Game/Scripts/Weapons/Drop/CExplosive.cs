﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CExplosive : MonoBehaviour {

    [Header("Bomb variables")]
    public bool _isExploded; // the bomb is already used?

    public float _lifeTime = 3; // time before disable

    public float _lifeTimer;

    [SerializeField]
    LayerMask _hitBoxesMask;

    [SerializeField]
    GameObject _parentHitBox; // to prevent the collision with parent

    [SerializeField, Header("Explosion Variables")]
    float _explosionPower = 10f;
    [SerializeField]
    float _explosionRadius = 5f;
    [SerializeField]
    float _explosionUpForce = 1f;

    [SerializeField]
    float _explosionDamage;

    [SerializeField, Header("To disable when hit")]
    GameObject[] _beforeExplosionObjects;

    [SerializeField, Header("To enable after hit")]
    GameObject[] _afterExplosionObjects;

    [SerializeField, Header("Sound Effects")]
    AudioSource _explosionSFX;

    protected virtual void Update()
    {
       
    }

    public void SetParentHitBox(GameObject aHitBox)
    {
        _parentHitBox = aHitBox;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_parentHitBox != null) // explosive weapons
        {
            if (other.gameObject != _parentHitBox && other.tag == CConstants.HITBOX_TAG && !_isExploded)
            {
                DoExplosion();
            }
        }
        else // to use with barrels
        {
            if (other.tag == CConstants.HITBOX_TAG && !_isExploded)
            {
                DoExplosion();
            }
        }
        
    }

    // explosion when die
    public void DoExplosion()
    {
        _explosionSFX.Play();

        _isExploded = true; // used flag

        // get colliders touched
        Collider[] tTouchedColliders = Physics.OverlapSphere(transform.position, _explosionRadius, _hitBoxesMask);
        // apply explosion forces
        foreach (Collider tHit in tTouchedColliders)
        {
            Rigidbody tRB = tHit.GetComponentInParent<Rigidbody>();
            if (tRB != null)
            {
                if (tRB.tag == CConstants.CARS_TAG)
                {
                    tRB.GetComponent<CCarHealth>().DoDamage(_explosionDamage); // do damage to other car
                }

                tRB.AddExplosionForce(_explosionPower, transform.position, _explosionRadius, _explosionUpForce, ForceMode.Impulse);
            }
        }

        // disable meshes
        foreach (GameObject tObject in _beforeExplosionObjects)
        {
            tObject.SetActive(false);
        }

        // Juice it
        foreach (GameObject tObject in _afterExplosionObjects)
        {
            tObject.SetActive(true);
        }

        CCameraShake._instance.DoEventShake();
    }

    public void ResetBomb()
    {
        // reset timer
        _lifeTimer = 0;

        // reset flag
        _isExploded = false;

        // disable meshes
        foreach (GameObject tObject in _beforeExplosionObjects)
        {
            tObject.SetActive(true);
        }

        // Juice it
        foreach (GameObject tObject in _afterExplosionObjects)
        {
            tObject.SetActive(false);
        }
    }
}
