﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CFlameThrower : CDamagePerTimeWerapon
{
    // specific code for Flamethrowers

    protected override void Start()
    {
        _spawnerType = State.FLAMETHROWER;
        base.Start();        
    }

    protected override void Update()
    {
        base.Update();
        // ...
    }
}
