﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CDamagePerTimeWerapon : MonoBehaviour {

    [SerializeField, Header("Config")]
    float _damagePerFrame;

    [SerializeField]
    ParticleSystem[] _particlesToEnableWhenIsFiring;

    [SerializeField]
    AudioSource _SFX;

    [SerializeField]
    GameObject _parentHitBox; // to prevent active fire with his own collider

    [SerializeField]
    CCar _parentCarScript; // to check if is dead

    public State _spawnerType;

    public enum State
    {
        FLAMETHROWER,
        CHAINSAW
    }

    protected virtual void Start()
    {
        _parentCarScript = _parentHitBox.GetComponentInParent<CCar>();
    }

    protected virtual void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject != _parentHitBox && other.tag == CConstants.HITBOX_TAG) // is the enemy car on target position?
        {
            if (!_parentCarScript.IsDead()) // if my car is not dead, FIRE!
            {
                // do damage to enemy
                other.GetComponentInParent<CCarHealth>().DoDamage(_damagePerFrame);

                // enable particles
                foreach (ParticleSystem tParticle in _particlesToEnableWhenIsFiring)
                {
                    if (!tParticle.isPlaying)
                    {
                        tParticle.Play();
                        _SFX.Play();
                    }                    
                }
            }            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject != _parentHitBox && other.tag == CConstants.HITBOX_TAG) // enemy leave the hit zone
        {
            // disable particles
            foreach (ParticleSystem tParticle in _particlesToEnableWhenIsFiring)
            {
                if (tParticle.isPlaying)
                {
                    tParticle.Stop();
                    _SFX.Stop();
                }
            }
        }
    }
}
