﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CSpikes : MonoBehaviour {

    [SerializeField, Header("Config")]
    float _impactDamage;

    [SerializeField]
    ParticleSystem _hitParticle;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == CConstants.HITBOX_TAG)
        {   
            // do damage
            other.transform.GetComponentInParent<CCarHealth>().DoDamage(_impactDamage);

            // juice it
            _hitParticle.Play();
            CCameraShake._instance.DoEventShake();
        }
    }
}
