﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CRocketSpawner : CExplosiveSpawner
{
    // specific code for projectile spawners

    protected override void Start()
    {
        _spawnerType = State.PROJECTILE;
        base.Start();

        // set spawner references
        for (int i = 0; i < _explosivesPool.Count; i++)
        {
            _explosivesPool[i].GetComponent<CRocket>()._spawnerReference = this;
        }
    }

    protected override void Update()
    {
        base.Update();
        // ...
    }
}
