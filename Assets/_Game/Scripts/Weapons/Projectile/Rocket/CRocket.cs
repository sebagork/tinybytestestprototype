﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CRocket : CExplosive {

    // to return it to the pool
    public CRocketSpawner _spawnerReference;

    private void FixedUpdate()
    {
        // move backward
        if (!_isExploded)
        {
            transform.Translate((Vector3.forward * 50) * Time.fixedDeltaTime, Space.World);
        }

        // check life time
        _lifeTimer += Time.deltaTime;
        if (_lifeTimer >= _lifeTime)
        {
            // return to the pool
            this.gameObject.SetActive(false);
            _spawnerReference.ReturnExplosiveToPool(this.gameObject);
        }
    }
}
