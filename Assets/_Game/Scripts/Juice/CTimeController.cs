﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CTimeController : MonoBehaviour {

    [SerializeField, Header("Time control variables")]
    float _slowDownFactor = 0.05f; // slowdown intensity

    [SerializeField]
    float _slowDownRecoverTime = 2f; // time to return to normal time

    [SerializeField]
    float _maxSlowDownTime = 1f; // time at max slow before start recover

    float _maxSlowTimer; // to control max slow time

    [SerializeField]
    float _defaultFixedUpdate;

    #region SINGLETON PATTERN
    public static CTimeController _instance = null;
    #endregion

    private void Awake()
    {
        // Singleton check
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        _defaultFixedUpdate = Time.fixedDeltaTime;
    }

    private void Update()
    {
        SlowMotionUpdate();
    }

    // control the slow motion
    void SlowMotionUpdate()
    {
        if (Time.deltaTime < 1)
        {
            if (_maxSlowTimer <= _maxSlowDownTime) // time on max slow
            {
                _maxSlowTimer += Time.unscaledDeltaTime;
            }
            else // start recovery
            {
                Time.timeScale += (1f / _slowDownRecoverTime) * Time.unscaledDeltaTime; // increase deltatime smoothly after slowdown
                Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f); // clamp deltatime
            }

            Time.fixedDeltaTime = _defaultFixedUpdate * Time.timeScale;
        }
    }

    // Start slow motion
    public void DoBulletTime()
    {
        Time.timeScale = _slowDownFactor;
        _maxSlowTimer = 0;
    }
}
