﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Events;

public class CCameraShake : MonoBehaviour
{
    // default shake when is not shaking by an event
    [SerializeField, Header("Normal state shake")]
    float _normalShakeAmplitude = 0.05f;
    [SerializeField]
    float _normalShakeFrequency = 0.3f;

    // shake when an event is triggered
    [SerializeField, Header("Shake config")]
    float _defaultShakeDuration = 0.3f; // Time the Camera Shake effect will last

    float _actualShakeDuration; // shake duration on this frame

    [SerializeField]
    float _defaultShakeAmplitude = 0.1f; // Cinemachine Noise Profile Parameter

    float _actualShakeAmplitude; // shake amplitude on this frame

    [SerializeField]
    public float _defaultShakeFrequency = 4f; // Cinemachine Noise Profile Parameter

    float _actualShakeFrequency; // Sgake frequency on this frame

    private float _shakeElapsedTime; // shake timer

    // Cinemachine Shake
    public CinemachineVirtualCamera _virtualCamera;
    private CinemachineBasicMultiChannelPerlin _virtualCameraNoise;

    #region SINGLETON PATTERN
    public static CCameraShake _instance = null;
    #endregion

    private void Awake()
    {
        // Singleton check
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        // first config
        _shakeElapsedTime = _defaultShakeDuration;

        // Get Virtual Camera Noise Profile
        if (_virtualCamera != null)
            _virtualCameraNoise = _virtualCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
    }

    // Update is called once per frame
    void Update()
    {
        ShakeUpdate();
    }

    public void DoEventShake(float aShakeAmplitude = 0, float aShakeFrequency = 0, float aShakeDuration = 0)
    {
        // config shake amplitude
        if (aShakeAmplitude != 0) // use value received at parameter
        {
            _actualShakeAmplitude = aShakeAmplitude;
        }
        else // use default
        {
            _actualShakeAmplitude = _defaultShakeAmplitude;
        }

        // config shake frequency
        if (aShakeFrequency != 0) // use value received at parameter
        {
            _actualShakeFrequency = aShakeFrequency;
        }
        else // use default
        {
            _actualShakeFrequency = _defaultShakeFrequency;
        }

        // config shake duration
        if (aShakeDuration != 0) // use value received at parameter
        {
            _actualShakeDuration = aShakeDuration;
        }
        else // use default
        {
            _actualShakeDuration = _defaultShakeDuration;
        }

        _shakeElapsedTime = 0;
    }

    // control the camera shake
    void ShakeUpdate()
    {
        // If the Cinemachine componet is not set, avoid update
        if (_virtualCamera != null && _virtualCameraNoise != null)
        {
            // If Camera Shake effect is still playing
            if (_shakeElapsedTime < _actualShakeDuration)
            {
                // Set Cinemachine Camera Noise parameters
                _virtualCameraNoise.m_AmplitudeGain = _actualShakeAmplitude;
                _virtualCameraNoise.m_FrequencyGain = _actualShakeFrequency;

                // Update Shake Timer
                _shakeElapsedTime += Time.deltaTime;
                //Time.timeScale += (1f / _slowDownLenght) * Time.unscaledDeltaTime; // increase deltatime smoothly after slowdown
            }
            else
            {
                // If Camera Shake effect is over, reset variables
                _virtualCameraNoise.m_AmplitudeGain = _normalShakeAmplitude;
                _virtualCameraNoise.m_FrequencyGain = _normalShakeFrequency;
                _shakeElapsedTime = _actualShakeDuration;
            }
        }
    }
}