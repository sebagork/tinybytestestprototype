﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CDeviceNeverSleep : MonoBehaviour {

	void Start () {
        // prevent the device sleep
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }
}
