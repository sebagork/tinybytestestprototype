﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CWheelsRotation : MonoBehaviour {

    // wheel rotation speed
    public float _rotationSpeed;

    // reference to all wheels
    [SerializeField]
    List<Transform> _wheelsTransforms;

    // wheels colliders
    [SerializeField]
    List<WheelCollider> _wheelColliders;

    void LateUpdate()
    {
        // adjust position of the wheels
        for (int i = 0; i < _wheelColliders.Count; i++)
        {
            Vector3 tWheelCCenter = _wheelColliders[i].transform.TransformPoint(_wheelColliders[i].center);

            RaycastHit tHit;
            if (Physics.Raycast(tWheelCCenter, -_wheelColliders[i].transform.up, out tHit, _wheelColliders[i].suspensionDistance + _wheelColliders[i].radius))
            {
                _wheelsTransforms[i].position = tHit.point + (_wheelColliders[i].transform.up * _wheelColliders[i].radius);
            }
            else
            {
                _wheelsTransforms[i].position = tWheelCCenter - (_wheelColliders[i].transform.up * _wheelColliders[i].suspensionDistance);
            }

            // rotate wheels
            _wheelsTransforms[i].Rotate(new Vector3(-_rotationSpeed * Time.deltaTime, 0, 0));
        }  
    }
}
