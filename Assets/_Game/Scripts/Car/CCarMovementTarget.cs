﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCarMovementTarget : MonoBehaviour {

    // to use as a limit when change the target position
    [SerializeField, Header("Change position config")]
    float _maxDistanceFromCenter;

    float _changePositionTimer; // to control the time between position changes

    float _changePositionDelay; // delay between position changes

    // max & min delay between position changes
    [SerializeField]
    float _maxDelay; 
    [SerializeField]
    float _minDelay;

    private void Start()
    {
        ChangePosition();
    }

    private void Update()
    {
        if (_changePositionTimer >= _changePositionDelay)
        {
            ChangePosition();
            _changePositionTimer = 0;         
        }

        _changePositionTimer += Time.deltaTime;
    }

    // change the position of the target
    public void ChangePosition()
    {
        transform.localPosition = new Vector3(Random.Range(-_maxDistanceFromCenter, _maxDistanceFromCenter), transform.localPosition.y, Random.Range(-_maxDistanceFromCenter, _maxDistanceFromCenter));
        _changePositionDelay = Random.Range(_minDelay, _maxDelay);
    }
}
