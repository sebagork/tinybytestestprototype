﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CCarHealth : MonoBehaviour {

    [SerializeField, Header("Car Variables")]
    float _maxHealth;
    [SerializeField]
    float _actualHealth;    

    // UI
    [SerializeField]
    Slider _healthSlider;

    // SFX
    [SerializeField, Header("Sound Effects")]
    AudioClip[] _crashEffects;

    [SerializeField]
    AudioSource _crashASource;

    private void Start()
    {
        // first config
        _actualHealth = _maxHealth;
        _healthSlider.value = _actualHealth;
    }

    // receive damage from enemy weapon
    public void DoDamage(float aDamage)
    {
        _actualHealth -= aDamage;
        _healthSlider.value = _actualHealth;

        // you are dead!
        if (_actualHealth <= 0)
        {
            Die();
        }
    }

    // sudden death
    public void SetHealthToZero()
    {
        _actualHealth = 0;
        _healthSlider.value = _actualHealth;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == CConstants.CARS_TAG)
        {
            CCameraShake._instance.DoEventShake();

            // random sound
            _crashASource.clip = _crashEffects[Random.Range(0, _crashEffects.Length)];
            _crashASource.Play();
        }
    }

    // when the car health reach 0
    [ContextMenu("DieTest")]
    public void Die()
    {
        // set dead state
        this.GetComponent<CCar>().Die();
    }
}
