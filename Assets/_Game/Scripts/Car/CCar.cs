﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCar : MonoBehaviour {

    [SerializeField, Header("Car Variables")]
    float _moveSpeed;

    Rigidbody _carRigidBody;

    [SerializeField]
    float _smoothRotationValue;

    [SerializeField]
    LayerMask _hitBoxesMask;    

    // target to follow
    [SerializeField, Header("Target config")]
    Transform _actualTarget;

    // two possible targets
    [SerializeField]
    Transform _movementTarget, _enemyTarget;

    float _changeTargetTimer; // to control the time between target changes

    float _changeTargetDelay; // delay between target changes

    // max & min delay between target changes
    [SerializeField]
    float _maxDelay;

    // when the car crash
    [SerializeField, Header("Crash dead config")] 
    float _crashDelay = 2; // time before die when crash

    [SerializeField]
    float _crashTimer = 0;

    [SerializeField, Header("Wheels")]
    WheelCollider[] _wheelsColliders;

    [SerializeField, Header("Dead Explosion Variables")]
    float _explosionPower = 10f;
    [SerializeField]
    float _explosionRadius = 5f;
    [SerializeField]
    float _explosionUpForce = 1f;

    [SerializeField]
    Transform[] _explosionPositionsList; // to randomize the dead "animation"

    [SerializeField, Header("Pull Backward")]
    float _deadPullBackwardForce = 0; // to simulate that the speed of the car is going down

    [SerializeField]
    float _deadPullBackwardMultiplier;

    [SerializeField, Header("Particles")]
    GameObject _explosionParticle;

    [SerializeField]
    GameObject _fireParticle;

    [SerializeField]
    GameObject[] _sandParticles;

    [SerializeField, Header("Sound Effects")]
    AudioSource _deadExplosion;

    enum State
    {
        GROUNDED,
        CRASHING,
        DEAD
    }

    [SerializeField, Header("State Machine")]
    State _state;

    private void Start()
    {
        // first config
        _crashTimer = 0;
        _deadPullBackwardForce = 0f;
        _carRigidBody = this.GetComponent<Rigidbody>();
        _carRigidBody.centerOfMass = new Vector3(0, -0.4f, 0.2f);
        _actualTarget = _movementTarget;
        SetState(State.GROUNDED);
    }

    private void FixedUpdate()
    {
        if (_state == State.GROUNDED)
        {
            FollowTarget(); // follow the actual target

            // do a smooth rotation to face the road
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(transform.rotation.x, Quaternion.identity.y, transform.rotation.y) , _smoothRotationValue * Time.deltaTime);

            if (!CheckGroundedWheels()) // if all wheels are ungrounded
            {
                SetState(State.CRASHING);
            }
        }
        else if (_state == State.CRASHING)
        {
            if (CheckGroundedWheels()) // if all wheels are ungrounded
            {
                SetState(State.GROUNDED);
            }

            if (Mathf.Abs(transform.localRotation.x) > 0.55 || Mathf.Abs(transform.localRotation.z) > 0.55)
            {
                _crashTimer += Time.fixedDeltaTime;                
            }
            else
            {
                _crashTimer = 0;
            }

            if (_crashTimer >= _crashDelay)
            {
                this.GetComponent<CCarHealth>().SetHealthToZero();
                SetState(State.DEAD);
            }            
        }
        else if (_state == State.DEAD)
        {
            _deadPullBackwardForce += Time.fixedDeltaTime * _deadPullBackwardMultiplier;
            transform.Translate((-Vector3.forward * _deadPullBackwardForce) * Time.fixedDeltaTime, Space.World);
        }        
    }

    // change car state
    void SetState(State aState)
    {
        _state = aState;
        
        if (_state == State.GROUNDED)
        {
            // set grounded variables
        }
        else if (_state == State.CRASHING)
        {
            // set crashing variables
        }
        else if (_state == State.DEAD)
        {
            // do Slow Motion
            CTimeController._instance.DoBulletTime();

            // BOOM!
            DoExplosion();

            // to do more action!!
            this.GetComponent<Rigidbody>().angularDrag = 0;

            // stop wheels models
            this.GetComponent<CWheelsRotation>()._rotationSpeed = 0;

            // enable dead particles
            _explosionParticle.SetActive(true);
            _fireParticle.SetActive(true);

            //disable sand particles
            foreach (GameObject tSandParticle in _sandParticles)
            {
                tSandParticle.SetActive(false);
            }            

            // fade out after dead
            CFadeController._instance.FadeOut();
        }
    }

    // aply force to follow the target smoothly
    public void FollowTarget()
    {
        Vector3 tDirection = (_actualTarget.position - transform.position).normalized;
        tDirection.y = 0; // maintain gravity
        _carRigidBody.velocity += tDirection * _moveSpeed * Time.deltaTime; // move car
    }

    // return true if all wheels are touching the ground
    public bool CheckGroundedWheels()
    {
        int tGroundedWheels = 0;
        foreach (WheelCollider tWheel in _wheelsColliders) 
        {
            if (tWheel.isGrounded)
            {
                tGroundedWheels++;
            }
        }

        if (tGroundedWheels == 4)
        {
            return true;
        }
        else
        {
            return false;
        }        
    }

    // Change the state machine to dead state
    public void Die()
    {
        if (_state != State.DEAD)
        {
            SetState(State.DEAD);
        }        
    }

    // return true is the car is dead
    public bool IsDead()
    {
        if (_state == State.DEAD)
            return true;
        else
            return false;
    }

    // explosion when die
    public void DoExplosion()
    {
        // sfx
        _deadExplosion.Play();

        // get random position from the list
        Vector3 tSelectedExplosionPosition = _explosionPositionsList[Random.Range(0, _explosionPositionsList.Length)].transform.position;
        // get colliders touched
        Collider[] tTouchedColliders = Physics.OverlapSphere(tSelectedExplosionPosition, _explosionRadius, _hitBoxesMask);
        // apply explosion forces
        foreach (Collider tHit in tTouchedColliders)
        {
            Rigidbody tRB = tHit.GetComponentInParent<Rigidbody>();
            if (tRB != null)
            {
                tRB.AddExplosionForce(_explosionPower, tSelectedExplosionPosition, _explosionRadius, _explosionUpForce, ForceMode.Impulse);
            }
        }
    }
}
