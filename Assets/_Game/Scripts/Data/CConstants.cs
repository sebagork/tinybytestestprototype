﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CConstants{
    // tags
    public const string CARS_TAG = "Car";
    public const string HITBOX_TAG = "HitBox";
}
