﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CRoadManager : MonoBehaviour {

    // road tiles
    [SerializeField, Header("Road Prefabs")]
    GameObject[] _roadTilesPrefabs;

    int _lastPrefabIndex = 0; // last spawned prefab

    // to add and remove road tiles acording with fightzone position
    [SerializeField, Header("References")]
    Transform _fightZoneTransform;

    float _spawnZ; // position of the next spawn 

    [SerializeField, Header("Tiles info")]
    float _tilesLenght = 100; // tiles lenght

    [SerializeField]
    int _activeTilesOnScreen = 5; // active tiles simultaneously

    [SerializeField]
    int _middleTileIndex; // index of middle tile in the list 

    [SerializeField, Header("Tiles Pool")]
    int _instancesPerTile; // how many copies of each prefab

    // to optimize performance
    [SerializeField]
    private List<GameObject> _tilesPool;

    [SerializeField]
    // references to the active tiles
    private List<GameObject> _activeTiles;

    private void Start()
    {
        // config all variables
        _fightZoneTransform = GameObject.FindGameObjectWithTag("FightZone").transform;

        _spawnZ = -_tilesLenght; // to spawn the first before the player

        _activeTiles = new List<GameObject>();

        // instance all roads tiles and add to the pool
        for (int i = 0; i < _roadTilesPrefabs.Length; i++)
        {
            for (int t = 0; t < _instancesPerTile; t++)
            {
                GameObject tTile = Instantiate(_roadTilesPrefabs[i]);
                tTile.transform.SetParent(this.transform);
                _tilesPool.Add(tTile);
            }
        }

        // spawn first tiles
        for (int i = 0; i < _activeTilesOnScreen; i++)
        {
            if (i < 4)
            {
                AddTileToTheRoad(0); // spawn safe zone
            }
            else
            {
                AddTileToTheRoad();
            }            
        }
    }

    private void Update()
    {
        // if the player is in the middle of the tiles, spawn next one.
        if (_fightZoneTransform.position.z > _activeTiles[_middleTileIndex].transform.position.z) //(_spawnZ - ((_activeTilesOnScreen * _tilesLenght) / 2)))
        {
            AddTileToTheRoad();
            RemoveTileFromTheRoad();
        }
    }

    // spawn a tile next to the last tile
    private void AddTileToTheRoad(int aPrefabIndex = - 1)
    {
        GameObject tTile;
        if (aPrefabIndex == -1)
            tTile = GetRandomTileFromPool(); // random tile
        else
            tTile = _tilesPool[0]; // safe tiles on start  
        
        tTile.transform.localPosition = Vector3.forward * _spawnZ;
        tTile.SetActive(true);
        _spawnZ += _tilesLenght;

        _tilesPool.Remove(tTile);
        _activeTiles.Add(tTile);       
    }

    // remove the first active tile and return it to the pool
    private void RemoveTileFromTheRoad()
    {
        _activeTiles[0].SetActive(false);
        _tilesPool.Add(_activeTiles[0]);
        _activeTiles.RemoveAt(0);
    }

    public GameObject GetRandomTileFromPool()
    {
        return _tilesPool[Random.Range(0, _tilesPool.Count)];
    }

    private int RandomPrefabIndex()
    {
        if (_roadTilesPrefabs.Length <= 1) // if there is only one prefab return the first one
            return 0;

        int tRandomIndex = Random.Range(0, _roadTilesPrefabs.Length);

        if (tRandomIndex == _lastPrefabIndex) // if the random selected prefab is the same that last spawned
        {
            while (tRandomIndex == _lastPrefabIndex) // keep searching another one
            {
                tRandomIndex = Random.Range(0, _roadTilesPrefabs.Length);
            }
        }        

        _lastPrefabIndex = tRandomIndex;
        return tRandomIndex;
    }
}
