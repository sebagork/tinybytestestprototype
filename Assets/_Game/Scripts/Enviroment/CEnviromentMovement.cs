﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CEnviromentMovement : MonoBehaviour {

    // enviroment move speed
    [SerializeField]
    float _moveSpeed;

    void FixedUpdate()
    {
        // move the fight zone
        transform.Translate((-transform.forward * _moveSpeed) * Time.fixedDeltaTime);
    }
}
