﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CFadeController : MonoBehaviour {

    [Tooltip("Fade duration")]
    public float _fadeTime = 2.0f;

    [Tooltip("Screen color at maximum fade")]
    public Color _fadeColor = new Color(0.01f, 0.01f, 0.01f, 1.0f);

    // if is true fade in when scene is started
    public bool _fadeOnStart = true;

    // is fading right now?
    private bool _isFading = false;

    // The render queue used by the fade mesh. Reduce this if you need to render on top of it.
    public int _renderQueue = 5000;

    // delay before fadeOut
    [SerializeField]
    float _fadeOutDelay;

    private float _uiFadeAlpha = 0;
    private MeshRenderer _fadeRenderer;
    private MeshFilter _fadeMesh;
    private Material _fadeMaterial = null;    

    public float _currentAlpha { get; private set; }

    #region SINGLETON PATTERN
    public static CFadeController _instance = null;
    #endregion      

    void Awake()
    {
        // Singleton check
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);

        // create the fade material
        _fadeMaterial = new Material(Shader.Find("Oculus/Unlit Transparent Color"));
        _fadeMesh = gameObject.AddComponent<MeshFilter>();
        _fadeRenderer = gameObject.AddComponent<MeshRenderer>();

        var tMesh = new Mesh();
        _fadeMesh.mesh = tMesh;

        Vector3[] vertices = new Vector3[4];

        float tWidth = 2f;
        float tHeight = 2f;
        float tDepth = 1f;

        vertices[0] = new Vector3(-tWidth, -tHeight, tDepth);
        vertices[1] = new Vector3(tWidth, -tHeight, tDepth);
        vertices[2] = new Vector3(-tWidth, tHeight, tDepth);
        vertices[3] = new Vector3(tWidth, tHeight, tDepth);

        tMesh.vertices = vertices;

        int[] tTri = new int[6];

        tTri[0] = 0;
        tTri[1] = 2;
        tTri[2] = 1;

        tTri[3] = 2;
        tTri[4] = 3;
        tTri[5] = 1;

        tMesh.triangles = tTri;

        Vector3[] tNormals = new Vector3[4];

        tNormals[0] = -Vector3.forward;
        tNormals[1] = -Vector3.forward;
        tNormals[2] = -Vector3.forward;
        tNormals[3] = -Vector3.forward;

        tMesh.normals = tNormals;

        Vector2[] tUV = new Vector2[4];

        tUV[0] = new Vector2(0, 0);
        tUV[1] = new Vector2(1, 0);
        tUV[2] = new Vector2(0, 1);
        tUV[3] = new Vector2(1, 1);

        tMesh.uv = tUV;

        _currentAlpha = 1;
        SetMaterialAlpha();
    }

    // Automatically starts a fade in
    void Start()
    {
        if (_fadeOnStart)
        {
            FadeIn();
        }
    }

    // Start a fade out
    public void FadeOut()
    {
        StartCoroutine(Fade(0, 1, _fadeOutDelay));
        Invoke("ReloadScene", _fadeTime + _fadeOutDelay); // reload scene after fade out
    }

    // start a fade in
    public void FadeIn()
    {
        StartCoroutine(Fade(1, 0));
    }

    // reload the scene
    public void ReloadScene()
    {
        SceneManager.LoadScene(0);
    }        

    // Fades alpha 
    IEnumerator Fade(float startAlpha, float endAlpha, float aDelayBeforeFade = 0)
    {
        yield return new WaitForSeconds(aDelayBeforeFade); // apply delay before fade

        float tElapsedTime = 0.0f;
        while (tElapsedTime < _fadeTime)
        {
            tElapsedTime += Time.deltaTime;
            _currentAlpha = Mathf.Lerp(startAlpha, endAlpha, Mathf.Clamp01(tElapsedTime / _fadeTime));
            SetMaterialAlpha();
            yield return new WaitForEndOfFrame();
        }
    }

    // Update material alpha
    private void SetMaterialAlpha()
    {
        Color tColor = _fadeColor;
        tColor.a = Mathf.Max(_currentAlpha, _uiFadeAlpha);
        _isFading = tColor.a > 0;
        if (_fadeMaterial != null)
        {
            _fadeMaterial.color = tColor;
            _fadeMaterial.renderQueue = _renderQueue;
            _fadeRenderer.material = _fadeMaterial;
            _fadeRenderer.enabled = _isFading;
        }
    }

    // Cleans up the fade material
    void OnDestroy()
    {
        if (_fadeRenderer != null)
            Destroy(_fadeRenderer);

        if (_fadeMaterial != null)
            Destroy(_fadeMaterial);

        if (_fadeMesh != null)
            Destroy(_fadeMesh);
    }
}
